﻿Toto cd obsahuje implementaci modulu pro získávání dat za pomoci dotazování se na linked data question answering.

Obsah:
|readme.txt ............ stručný popis obsahu CD
|-src
| |-impl ............... zdrojové kódy implementace
| |-thesis ............. zdrojová forma práce ve formátu latex
|-text ................. text práce
  |-thesis.pdf ......... text práce ve formátu PDF
  |-thesis.ps .......... text práce ve formátu PS